# -*- coding: utf-8 -*-
import jieba
import codecs
import numpy as np
from utils import Vocabulary
from utils import calculate_mutual_information
from sklearn.model_selection import KFold
import re
import unicodedata
from langconv import *
from sklearn.model_selection import KFold

# 繁体->简体
def tradition2simple(line):
    line = Converter('zh-hans').convert(line)
    # line = line.encode('utf-8')
    return line

# 大写->小写，中文标点->英文
def lower_and_symbol(content):
    table = {ord(f): ord(t) for f, t in zip(
        u'，。！？【】《》（）％＃＠＆１２３４５６７８９０¥',
        u',.!?[]<>()%#@&1234567890$')}
    content = content.lower()
    content = unicodedata.normalize("NFKC", content)
    content = content.translate(table)

    return content

# 去除空格
def deleteBlank(content):
    content = content.replace('\r',' ').replace('\t',' ').replace('\n',' ').strip()
    return content

# 去除点
def deleteDoc(content):
    r = '[.【 】/《 》=☆*}{]'
    #●（）。:！
    temp = re.findall(r, content)
    content = re.sub(r, "", content)
    content = content + ' ' + " ".join(temp)
    return content


# read and regularize train data
def read_train_data():
    file_path = '../raw_data/带标签短信.txt'
    label = []
    train = []
    with codecs.open(file_path, 'r', encoding='utf-8') as f:
        for line in f:
            line = line.strip()
            label.append(line[0])
            content = lower_and_symbol(line[1:].lstrip())
            content = deleteBlank(content)
            content = deleteDoc(content)
            content = tradition2simple(content)

            train.append(content)
    return train, label

# read and regularize test data
def read_test_data():
    file_path = '../raw_data/不带标签短信.txt'
    test = []
    with codecs.open(file_path, 'r', 'utf-8') as f:
        for line in f.read().split('\n')[:-1]:
            line = line.strip()
            content = lower_and_symbol(line[:].lstrip())
            content = deleteBlank(content)
            content = deleteDoc(content)
            content = tradition2simple(content)
            test.append(content)
    return test

# tokenize data
def tokenize(train):
    tokens = []
    for content in train:

        token = jieba.cut(content, cut_all=False, HMM=True)
        tokens.append([t for t in token])

    print(len(tokens))
    return tokens

def build_account_oov(corpus, vocab):
    account = {}
    oov = {}
    for sen in corpus:
        for word in sen:
            if word in vocab:
                if word not in account:
                    account[word] = 1
                else:
                    account[word] += 1
            else:
                if word not in oov:
                    oov[word] = 1
                else:
                    oov[word] += 1

    sorted_account = sorted(account.items(), key=lambda item: item[1], reverse=True)
    sorted_oov = sorted(oov.items(), key=lambda item: item[1], reverse=True)
    list_account = [list(word) for word in sorted_account]
    list_oov = [list(word) for word in sorted_oov]

    return list_oov, list_account

# can be ignore
# build vocabulary for Tencent embedding
def build_vocab(corpus):
    embed = []

    corpus_token_set = set(token for content in corpus for token in content)
    vocab = Vocabulary()
    vocab.add('<digit>')
    alpha = 0.5 * (2.0 * np.random.random() - 1.0)
    curr_embed = (2.0 * np.random.random_sample([300]) - 1.0) * alpha
    embed.append(curr_embed)
    with open('./Tencent_AILab_ChineseEmbedding.txt', encoding='utf-8') as f:
        for l in f:
            l = l.strip().split(" ")
            word = l[0]
            vector = l[1:]
            if word in corpus_token_set:
                vocab.add(word)
                embed.append(np.fromiter((float(e) for e in vector), dtype=np.float))

    # oov_set = corpus_token_set.difference(set(vocab.tokens))
    embed = np.array(embed)

    return embed, vocab


def create_k_fold_set(dataset_path, train, train_idx, label, tokenize_train):
    kf = KFold(n_splits=5, shuffle=True, random_state=0)
    for i in range(5):
        with codecs.open(dataset_path + 'train_%d.txt' % (i + 1), 'w', encoding="utf-8") as train_file, codecs.open(
                dataset_path + 'dev_%d.txt' % (i + 1), 'w', encoding="utf-8") as dev_file:
            for tr_idx, dev_idx in kf.split(train_idx):
                for i in tr_idx:
                    train_file.write(",\t".join([label[i], train[i], train_idx[i], tokenize_train[i]]))
                    train_file.write("\n")
                for i in dev_idx:
                    dev_file.write(",\t".join([label[i], train[i], train_idx[i], tokenize_train[i]]))
                    dev_file.write("\n")

# remove digit
def cleaning(corpus):
    pattern = re.compile(r"\d+\.?\d*")
    for doc in corpus:
        for idx, token in enumerate(doc):
            if re.match(pattern, token):
                doc[idx] = '<digit>'


def transform_corpus(corpus, vocab):
    return [' '.join(str(vocab.get_id(token)) for token in doc if token in vocab)  for doc in corpus]





