# -*- coding: utf-8 -*-

import numpy as np
from gensim.models import word2vec
import preprocess
import codecs
import os

data_path = '../data/'
dataset_path = '../dataset/'
if not os.path.exists(dataset_path):
    os.makedirs(dataset_path)

if not os.path.exists(data_path):
    os.makedirs(data_path)

# read and regularize data
train, label = preprocess.read_train_data()
train_token = preprocess.tokenize(train)

test = preprocess.read_test_data()
test_token = preprocess.tokenize(test)

preprocess.cleaning(train_token)
preprocess.cleaning(test_token)

# generate word2vector
model = word2vec.Word2Vec(sentences=train_token+test_token, min_count=1)
model.save(data_path + 'w2v.model')

word2vec_model = word2vec.Word2Vec.load(data_path + 'w2v.model')
# vocab = ['我', '是', '一个', '学生', ...]
vocab = word2vec_model.wv.vocab
# vocab_idx={'我':0, '是':1, ...}
vocab_idx = {}
# word_vector=[[0.5,...,0.9],...]
word_vector = []

# build oov and account
oov, train_account = preprocess.build_account_oov(train_token, vocab)


with codecs.open(data_path + "word2vec_account.txt",'w', encoding="utf-8") as f:
    for word in train_account:
        f.write("%s\t%d\n" % (word[0], word[1]))

with codecs.open(data_path + "OOV.txt",'w', encoding="utf-8") as f:
    for word in oov:
        f.write("%s\t%d\n" % (word[0], word[1]))

i = 0
for word in vocab:
    word_vector.append(word2vec_model[word])
    vocab_idx[word] = str(i)
    i += 1

with codecs.open(data_path + "word2vec_vocab.txt", "w", encoding="utf-8") as f:
    for word in vocab_idx:
        f.write("%s\t%s\n" % (vocab_idx[word], word))

np.save(data_path + "word2vec_embedding.npy", word_vector)

train_idx = np.array([" ".join(vocab_idx[word] for word in sen if word in vocab_idx) for sen in train_token])
test_idx = np.array([" ".join(vocab_idx[word] for word in sen if word in vocab_idx) for sen in test_token])

tokenize_train = np.array([" ".join(word for word in sen if word in vocab_idx) for sen in train_token])
tokenize_test = np.array([" ".join(word for word in sen if word in vocab_idx) for sen in test_token])


with codecs.open(dataset_path + "train.txt", "w", encoding="utf-8") as f:
    f.write("label,\tcontent,\tvocab_idx,\ttokenize_content\n")
    for i in range(len(label)):
        f.write("%s,\t%s,\t%s,\t%s\n" % (label[i], train[i], train_idx[i], tokenize_train[i]))

with codecs.open(dataset_path + "test.txt", "w", encoding="utf-8") as f:
    f.write("content,\tvocab_idx,\ttokenize_content\n")
    for i in range(len(test)):
        f.write("%s,\t%s,\t%s\n" % (test[i], test_idx[i], tokenize_test[i]))

# 5-fold cross validation
# write into train file and dev file
preprocess.create_k_fold_set(dataset_path, train, train_idx, label, tokenize_train)



