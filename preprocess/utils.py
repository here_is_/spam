import numpy as np
import math

class Vocabulary:
    r""" Vocabulary class, an mapping between token and id. 
    Quickly construct, load, and save vocabulary for corpus.
    
    Functions
    ------------
    add : add an token into the vocabulary.

    get_id : get token's id

    get_token : get the token corresponding to given id

    tokens(property) : return list of tokens
    
    load : an classmethod, load a vocabulary from an text file, return corresponding vocabulary instance.

    save : save the vocabulary instance into an text file.
    """
    def __init__(self):
        self.id2token = []
        self.token2id = {}
    
    def add(self, token):
        """add an token into vocabulary.
        """
        if token not in self:
            self.id2token.append(token)
            self.token2id[token] = len(self.token2id)
    
    def get_id(self, token):
        """get token's id
        """
        return self.token2id[token]
    
    def get_token(self, id_):
        """ get the token corresponding to given id
        """
        return self.id2token[id_]
         
    def __contains__(self, token):
        return token in self.token2id
    
    def __len__(self):
        return len(self.id2token)
    
    @property
    def tokens(self):
        return self.id2token

    def save(self, path):
        """
        save the vocabulary instance into an text file.
        """
        with open(path, 'w', encoding='utf8') as f:
            f.write('id\ttoken\n')
            f.write('\n'.join('{}\t{}'.format(id_, token) for id_, token in enumerate(self.id2token)))
    
    @classmethod
    def load(cls, path):
        """ 
        load a vocabulary from an text file, return corresponding vocabulary instance.
        """
        vocab = cls()
        with open(path, 'r', encoding='utf8') as f:
            next(f)
            for line in f:
                s_line = line
                line = line.strip()
                if not line: continue
                # fields = line.split('\t')
                # if len(fields) > 1:
                #     token = fields[1]
                # else:
                #     token = ''
                id_, token = line.split('\t')
                vocab.add(token)
        return vocab


def calculate_mutual_information(vocab, corpus, label_idx):
    r"""Calculate mutual information between given class and every tokens.

    Parameters
    -------------
    vocab : the vocabulary of given corpus.

    corpus : iterator of docs. The doc is an iterator of tokens.

    label_idx : the indexes of docs belonged to the given class, an list or array.

    """
    # construct doc-token-co-occurence matrix
    occurence_matrix = np.zeros((len(corpus), len(vocab)))
    for idx, doc in enumerate(corpus):
        for token in doc:
            if token in vocab:
                occurence_matrix[idx, vocab.get_id(token)] = 1.0

    tokens_mutual_info = np.zeros(len(vocab))

    # start calcualte all words mutual information with given class
    N = len(corpus) + 4
    for token in range(len(vocab)):
        sta_matrix = np.zeros((2,2))
        sta_matrix[0, 0] = np.sum(occurence_matrix[label_idx, token])
        sta_matrix[0, 1] = np.sum(occurence_matrix[:, token]) - sta_matrix[0, 0]
        sta_matrix[1, 0] = len(label_idx) - sta_matrix[0, 0]
        sta_matrix[1, 1] = len(corpus) - len(label_idx) - sta_matrix[0, 1]
        sta_matrix += 1  # in case of dividing by 0.
        value = 0
        for t in (0, 1):
            for l in (0, 1):
                value += (sta_matrix[t, l] / N) * math.log2((sta_matrix[t, l] * N) / (np.sum(sta_matrix[t, :]) * np.sum(sta_matrix[:, l])))
        tokens_mutual_info[token] = value
    
    # return topn tokens
    topn_tokens_ids = np.argsort(tokens_mutual_info)[::-1]
    topn_tokens = [vocab.get_token(id_) for id_ in topn_tokens_ids]

    return topn_tokens


